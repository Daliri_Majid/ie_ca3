FROM maven:3.8.1-jdk-11-slim AS build
COPY . /home/app
RUN cd /home/app && mvn clean package

FROM tomcat:9.0.46-jdk11-openjdk-slim
RUN rm -rf /usr/local/tomcat/webapps/*
COPY --from=build /home/app/target/ie_ca3.war /usr/local/tomcat/webapps/ROOT.war
EXPOSE 8080
CMD ["catalina.sh","run"]