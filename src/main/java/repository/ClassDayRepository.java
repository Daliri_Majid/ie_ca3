package repository;

import java.sql.*;
import java.util.ArrayList;

class ClassDayRepository {
    private ClassDayRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        Statement createTableStatement = con.createStatement();
        createTableStatement.executeUpdate(
                "CREATE TABLE IF NOT EXISTS ClassDay\n" +
                        "(\n" +
                        " code      char(7) NOT NULL,\n" +
                        " classCode char(2) NOT NULL,\n" +
                        " day       varchar(50) NOT NULL,\n" +
                        " PRIMARY KEY ( code, classCode, day ),\n" +
                        " FOREIGN KEY ( code, classCode ) REFERENCES Course ( code, classCode )\n" +
                        ");"
        );
        createTableStatement.close();
        con.close();
    }

    public static ClassDayRepository getInstance() {
        if (instance == null)
            try {
                instance = new ClassDayRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in ClassDayRepository.create query.");
            }
        return instance;
    }

    public void insert(String code, String classCode, String day) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "INSERT INTO ClassDay(code, classCode, day) " +
                        "VALUES(?, ?, ?);"
        );
        st.setString(1, code);
        st.setString(2, classCode);
        st.setString(3, day);
        st.execute();
        st.close();
        con.close();
    }

    public ArrayList<String> find(String code, String classCode) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT day FROM ClassDay classDay WHERE classDay.code = ? AND classDay.classCode = ?;"
        );
        st.setString(1, code);
        st.setString(2, classCode);
        ResultSet rs = st.executeQuery();
        ArrayList<String> days = new ArrayList<>();
        while (rs.next())
            days.add(rs.getString(1));
        st.close();
        con.close();
        return days;
    }

    private static ClassDayRepository instance;
}
