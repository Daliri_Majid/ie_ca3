package repository;

import model.WeeklyScheduleItem;

import java.sql.*;
import java.util.ArrayList;

public class WeeklyScheduleItemRepository {
    private WeeklyScheduleItemRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        Statement createTableStatement = con.createStatement();
        createTableStatement.executeUpdate(
                "CREATE TABLE IF NOT EXISTS WeeklyScheduleItem\n" +
                        "(\n" +
                        " code      char(7) NOT NULL,\n" +
                        " classCode char(2) NOT NULL,\n" +
                        " studentId char(9) NOT NULL,\n" +
                        " status    varchar(50) NOT NULL,\n" +
                        " action    varchar(50) NOT NULL,\n" +
                        " PRIMARY KEY ( code, classCode, studentId ),\n" +
                        " FOREIGN KEY ( code, classCode ) REFERENCES Course ( code, classCode ),\n" +
                        " FOREIGN KEY ( studentId ) REFERENCES Student ( id )\n" +
                        ");"
        );
        createTableStatement.close();
        con.close();
    }

    public static WeeklyScheduleItemRepository getInstance() {
        if (instance == null)
            try {
                instance = new WeeklyScheduleItemRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in WeeklyScheduleItem.create query.");
            }
        return instance;
    }

    public int getSumUnits(String studentId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT SUM(units) FROM WeeklyScheduleItem item, Course course " +
                        "WHERE item.action <> ? AND item.studentId = ? AND item.code = course.code" +
                        " AND item.classCode = course.classCode;"
        );
        st.setString(1, WeeklyScheduleItem.getActionString(WeeklyScheduleItem.Action.REMOVE));
        st.setString(2, studentId);
        ResultSet rs = st.executeQuery();
        if (!rs.next())
        {
            st.close();
            con.close();
            return 0;
        }
        int result = rs.getInt(1);
        st.close();
        con.close();
        return result;
    }

    public void insert(WeeklyScheduleItem item) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "INSERT INTO WeeklyScheduleItem(code, classCode, studentId, status, action) " +
                        "VALUES(?,?,?,?,?);"
        );
        st.setString(1, item.getCode());
        st.setString(2, item.getClassCode());
        st.setString(3, item.getStudentId());
        st.setString(4, item.getStatus());
        st.setString(5, item.getAction());
        st.execute();
        st.close();
        con.close();
    }

    public ArrayList<WeeklyScheduleItem> findAllItems(String studentId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT * FROM WeeklyScheduleItem item " +
                        "WHERE item.studentId = ?;"
        );
        st.setString(1, studentId);
        ResultSet rs = st.executeQuery();
        var items = new ArrayList<WeeklyScheduleItem>();
        while (rs.next())
            items.add(getModelFromResultSet(rs));
        st.close();
        con.close();
        return items;
    }

    public ArrayList<WeeklyScheduleItem> findFinalItems(String studentId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT * FROM WeeklyScheduleItem item " +
                        "WHERE item.studentId = ? AND item.status <> ?;"
        );
        st.setString(1, studentId);
        st.setString(2, WeeklyScheduleItem.getStatusString(WeeklyScheduleItem.Status.NON_FINALIZED));
        ResultSet rs = st.executeQuery();
        var items = new ArrayList<WeeklyScheduleItem>();
        while (rs.next())
            items.add(getModelFromResultSet(rs));
        st.close();
        con.close();
        return items;
    }
    
    public int findSignedUpNumber(String code, String classCode) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT COUNT(*) FROM WeeklyScheduleItem item " +
                        "WHERE item.status <> ? AND item.code = ? AND item.classCode = ?;"
        );
        st.setString(1, WeeklyScheduleItem.getStatusString(WeeklyScheduleItem.Status.NON_FINALIZED));
        st.setString(2, code);
        st.setString(3, classCode);
        ResultSet rs = st.executeQuery();
        if (!rs.next())
        {
            st.close();
            con.close();
            return 0;
        }
        int result = rs.getInt(1);
        st.close();
        con.close();
        return result;
    }

    public ArrayList<CourseKey> findAllCourse(String studentId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT code, classCode FROM WeeklyScheduleItem item WHERE item.studentId = ?;"
        );
        st.setString(1, studentId);
        ResultSet rs = st.executeQuery();
        ArrayList<CourseKey> courseIDs = new ArrayList<>();
        while (rs.next())
            courseIDs.add(new CourseKey(rs.getString(1), rs.getString(2)));
        st.close();
        con.close();
        return courseIDs;
    }

    public WeeklyScheduleItem find(String code, String classCode, String studentId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT status, action FROM WeeklyScheduleItem item WHERE item.code = ? AND item.classCode = ? " +
                        "AND item.studentId = ?;"
        );
        st.setString(1, code);
        st.setString(2, classCode);
        st.setString(3, studentId);
        ResultSet resultSet = st.executeQuery();
        if (!resultSet.next()) {
            st.close();
            con.close();
            return null;
        }
        var item = new WeeklyScheduleItem(code, classCode, studentId, resultSet.getString(1), resultSet.getString(2));
        st.close();
        con.close();
        return item;
    }

    public void remove(String code, String classCode, String studentId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "DELETE FROM WeeklyScheduleItem item WHERE item.code = ? AND item.classCode = ? " +
                        "AND item.studentId = ?;"
        );
        st.setString(1, code);
        st.setString(2, classCode);
        st.setString(3, studentId);
        st.execute();
        st.close();
        con.close();
    }

    public void updateAction(String code, String classCode, String studentId,
                             String newAction) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "UPDATE  WeeklyScheduleItem item " +
                        "SET item.action = ? " +
                        "WHERE item.code = ? AND item.classCode = ? AND item.studentId = ?;"
        );
        st.setString(1, newAction);
        st.setString(2, code);
        st.setString(3, classCode);
        st.setString(4, studentId);
        st.executeUpdate();
        st.close();
        con.close();
    }

    public void updateStatus(String code, String classCode, String studentId,
                             String newStatus) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "UPDATE  WeeklyScheduleItem item " +
                        "SET item.status = ? " +
                        "WHERE item.code = ? AND item.classCode = ? AND item.studentId = ?;"
        );
        st.setString(1, newStatus);
        st.setString(2, code);
        st.setString(3, classCode);
        st.setString(4, studentId);
        st.executeUpdate();
        st.close();
        con.close();
    }

    public void updateActionStatus(String code, String classCode, String studentId,
                             String newAction, String newStatus) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "UPDATE  WeeklyScheduleItem item " +
                        "SET item.action = ?, item.status = ?" +
                        "WHERE item.code = ? AND item.classCode = ? AND item.studentId = ?;"
        );
        st.setString(1, newAction);
        st.setString(2, newStatus);
        st.setString(3, code);
        st.setString(4, classCode);
        st.setString(5, studentId);
        st.executeUpdate();
        st.close();
        con.close();
    }

    public ArrayList<WeeklyScheduleItem> findAllWaitings() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT * FROM WeeklyScheduleItem item " +
                        "WHERE item.status = ?;"
        );
        st.setString(1, WeeklyScheduleItem.getStatusString(WeeklyScheduleItem.Status.WAITING));
        ResultSet rs = st.executeQuery();
        var items = new ArrayList<WeeklyScheduleItem>();
        while (rs.next())
            items.add(getModelFromResultSet(rs));
        st.close();
        con.close();
        return items;
    }

    private WeeklyScheduleItem getModelFromResultSet(ResultSet rs) throws SQLException {
        return new WeeklyScheduleItem(rs.getString(1), rs.getString(2), rs.getString(3),
                rs.getString(4), rs.getString(5));
    }

    private static WeeklyScheduleItemRepository instance = null;
}
