package repository;

import model.Lesson;

import java.sql.*;

public class LessonRepository {
    private LessonRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        Statement createTableStatement = con.createStatement();
        createTableStatement.executeUpdate(
                "CREATE TABLE IF NOT EXISTS Lesson\n" +
                        "(\n" +
                        " code  char(7) NOT NULL,\n" +
                        " units int NOT NULL,\n" +
                        " name  varchar(50) NOT NULL,\n" +
                        " PRIMARY KEY ( code )\n" +
                        ");"
        );
        createTableStatement.close();
        con.close();
    }

    public static LessonRepository getInstance() {
        if (instance == null)
            try {
                instance = new LessonRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in LessonRepository.create query.");
            }
        return instance;
    }

    public Lesson find(String code) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT * FROM Lesson lesson WHERE lesson.code = ?;"
        );
        st.setString(1, code);
        ResultSet rs = st.executeQuery();
        if (!rs.next()) {
            st.close();
            con.close();
            return null;
        }
        Lesson lesson = new Lesson(rs.getString(1), rs.getInt(2), rs.getString(3));
        st.close();
        con.close();
        return lesson;
    }

    public void insert(Lesson lesson) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "INSERT INTO Lesson(code, units, name) " +
                        "VALUES(?, ?, ?);"
        );
        st.setString(1, lesson.getCode());
        st.setInt(2, lesson.getUnits());
        st.setString(3, lesson.getName());
        st.execute();
        st.close();
        con.close();
    }

    private static LessonRepository instance;
}
