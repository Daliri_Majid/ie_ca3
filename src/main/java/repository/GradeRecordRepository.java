package repository;

import model.GradeRecord;

import java.sql.*;
import java.util.ArrayList;

public class GradeRecordRepository {
    private GradeRecordRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        Statement createTableStatement = con.createStatement();
        createTableStatement.executeUpdate(
                "CREATE TABLE IF NOT EXISTS GradeRecord\n" +
                        "(\n" +
                        " lessonCode char(7) NOT NULL,\n" +
                        " studentId  char(9) NOT NULL,\n" +
                        " term       int NOT NULL,\n" +
                        " grade      float NOT NULL,\n" +
                        " PRIMARY KEY ( lessonCode, studentId, term, grade ),\n" +
                        " FOREIGN KEY ( lessonCode ) REFERENCES Lesson ( code ),\n" +
                        " FOREIGN KEY ( studentId, term ) REFERENCES TermRecord ( studentId, term )\n" +
                        ");"
        );
        createTableStatement.close();
        con.close();
    }

    public static GradeRecordRepository getInstance() {
        if (instance == null)
            try {
                instance = new GradeRecordRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in StudentRepository.create query.");
            }
        return instance;
    }

    public ArrayList<GradeRecord> findAllInTerm(String studentId, int term) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT * FROM GradeRecord record " +
                        "WHERE record.studentId = ? AND record.term = ?;"
        );
        st.setString(1, studentId);
        st.setInt(2, term);
        ResultSet rs = st.executeQuery();
        var grades = new ArrayList<GradeRecord>();
        while (rs.next())
            grades.add(new GradeRecord(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getFloat(4)));
        st.close();
        con.close();
        return grades;
    }

    public GradeRecord findLast(String code, String studentId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT term, grade FROM GradeRecord record" +
                        " WHERE record.lessonCode = ? AND record.studentId = ? AND record.term IN" +
                        " (SELECT MAX(term) FROM GradeRecord record2" +
                        " WHERE record2.lessonCode = ? AND record2.studentId = ?);"
        );
        st.setString(1, code);
        st.setString(2, studentId);
        st.setString(3, code);
        st.setString(4, studentId);
        ResultSet rs = st.executeQuery();
        if (!rs.next()) {
            st.close();
            con.close();
            return null;
        }
        var record = new GradeRecord(code, studentId, rs.getInt(1), rs.getFloat(2));
        st.close();
        con.close();
        return record;
    }

    public void insert(String lessonCode, String studentId, int termNumber, float grade) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "INSERT INTO GradeRecord(lessonCode, studentId, term, grade) " +
                        "VALUES(?,?,?,?);"
        );
        st.setString(1, lessonCode);
        st.setString(2, studentId);
        st.setInt(3, termNumber);
        st.setFloat(4, grade);
        st.execute();
        st.close();
        con.close();
    }

    private static GradeRecordRepository instance;
}
