package repository;

public class CourseKey {
    public CourseKey(String code, String classCode) {
        this.code = code;
        this.classCode = classCode;
    }

    public String code;
    public String classCode;
}
