package repository;

import model.Course;

import java.sql.*;
import java.util.ArrayList;

public class CourseRepository {
    private CourseRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        Statement createTableStatement = con.createStatement();
        createTableStatement.executeUpdate(
                "CREATE TABLE IF NOT EXISTS Course\n" +
                        "(\n" +
                        " code       char(7) NOT NULL,\n" +
                        " classCode  char(2) NOT NULL,\n" +
                        " name       varchar(50) NOT NULL,\n" +
                        " units      int NOT NULL,\n" +
                        " type       varchar(50) NOT NULL,\n" +
                        " instructor varchar(50) NOT NULL,\n" +
                        " capacity   int NOT NULL,\n" +
                        " examStart  varchar(50) NOT NULL,\n" +
                        " examEnd    varchar(50) NOT NULL,\n" +
                        " classTime  varchar(50) NOT NULL,\n" +
                        " PRIMARY KEY ( code, classCode ),\n" +
                        " FOREIGN KEY ( code ) REFERENCES Lesson ( code )\n" +
                        ");"
        );
        createTableStatement.close();
        con.close();
    }

    public static CourseRepository getInstance() {
        if (instance == null)
            try {
                instance = new CourseRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in CourseRepository.create query.");
            }
        return instance;
    }

    public ArrayList<Course> findNewCourses(String studentId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT * FROM Course course WHERE course.code NOT IN " +
                        "(SELECT grade.lessonCode FROM GradeRecord grade WHERE " +
                        "grade.studentId = ? AND grade.lessonCode = course.code AND grade.grade >= 10);"
        );
        st.setString(1, studentId);
        ResultSet rs =  st.executeQuery();
        ArrayList<Course> courses = new ArrayList<>();
        while (rs.next())
            courses.add(convertResultSetToDomainModel(rs));
        st.close();
        con.close();
        return courses;
    }

    public void insert(Course course) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "INSERT INTO Course(code, classCode, name, units, type, instructor, capacity, examStart," +
                        "examEnd, classTime) " +
                        "VALUES(?,?,?,?,?,?,?,?,?,?);"
        );
        st.setString(1, course.getCode());
        st.setString(2, course.getClassCode());
        st.setString(3, course.getName());
        st.setInt(4, course.getUnits());
        st.setString(5, course.getType());
        st.setString(6, course.getInstructor());
        st.setInt(7, course.getCapacity());
        st.setString(8, course.getExamTime().getStart());
        st.setString(9, course.getExamTime().getEnd());
        st.setString(10, course.getClassTime().getTime());
        st.execute();
        st.close();
        con.close();
        for (String day : course.getClassTime().getDays())
            ClassDayRepository.getInstance().insert(course.getCode(), course.getClassCode(), day);
    }

    public Course find(String code, String classCode) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT * FROM Course course WHERE course.code = ? AND course.classCode = ?;"
        );
        st.setString(1, code);
        st.setString(2, classCode);
        ResultSet rs = st.executeQuery();
        if (!rs.next()) {
            st.close();
            con.close();
            return null;
        }
        var course = convertResultSetToDomainModel(rs);
        st.close();
        con.close();
        return course;
    }

    private Course convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        Course course = new Course(rs.getString(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5),
                rs.getString(6), rs.getInt(7), rs.getString(8), rs.getString(9), rs.getString(10));
        ArrayList<String> days = ClassDayRepository.getInstance().find(course.getCode(), course.getClassCode());
        course.setClassDays(days);
        return course;
    }

    private static CourseRepository instance;
}
