package repository;

import model.Student;

import java.sql.*;

public class StudentRepository {
    private StudentRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        Statement createTableStatement = con.createStatement();
        createTableStatement.executeUpdate(
                "CREATE TABLE IF NOT EXISTS Student " +
                        "(\n" +
                        " id             char(9) NOT NULL,\n" +
                        " name       varchar(50) NOT NULL,\n" +
                        " secondName varchar(50) NOT NULL,\n" +
                        " email varchar(50) NOT NULL UNIQUE,\n" +
                        " password varchar(255) NOT NULL,\n" +
                        " birthDate  varchar(50) NOT NULL,\n" +
                        " field      varchar(50) NOT NULL,\n" +
                        " faculty    varchar(50) NOT NULL,\n" +
                        " level      varchar(50) NOT NULL,\n" +
                        " status     varchar(50) NOT NULL,\n" +
                        " image      varchar(255) NOT NULL,\n" +
                        " PRIMARY KEY (id)" +
                        ");"
        );
        createTableStatement.close();
        con.close();
    }

    public static StudentRepository getInstance() {
        if (instance == null)
            try {
                instance = new StudentRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in StudentRepository.create query.");
            }
        return instance;
    }

    public void insert(Student student) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "INSERT INTO Student(id, name, secondName, email, password, birthDate, field, faculty, " +
                        "level, status, image) " +
                        "VALUES(?,?,?,?,?,?,?,?,?,?,?);"
        );
        st.setString(1, student.getId());
        st.setString(2, student.getName());
        st.setString(3, student.getSecondName());
        st.setString(4, student.getEmail());
        st.setString(5, student.getPassword());
        st.setString(6, student.getBirthDate());
        st.setString(7, student.getField());
        st.setString(8, student.getFaculty());
        st.setString(9, student.getLevel());
        st.setString(10, student.getStatus());
        st.setString(11, student.getImage());
        st.execute();
        st.close();
        con.close();
    }

    public void updatePassword(String email, String password) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "UPDATE  Student student " +
                        "SET student.password = ? " +
                        "WHERE student.email = ?;"
        );
        st.setString(1, password);
        st.setString(2, email);
        st.executeUpdate();
        st.close();
        con.close();
    }

    public Student findByEmail(String email) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT * FROM Student student " +
                        "WHERE email = ?;"
        );
        st.setString(1, email);
        ResultSet resultSet = st.executeQuery();
        if (!resultSet.next()) {
            st.close();
            con.close();
            return null;
        }
        Student result = convertResultSetToDomainModel(resultSet);
        st.close();
        con.close();
        return result;
    }

    public Student findById(String id) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT * FROM Student student " +
                        "WHERE id = ?;"
        );
        st.setString(1, id);
        ResultSet resultSet = st.executeQuery();
        if (!resultSet.next()) {
            st.close();
            con.close();
            return null;
        }
        Student result = convertResultSetToDomainModel(resultSet);
        st.close();
        con.close();
        return result;
    }

    private Student convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return new Student(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10), rs.getString(11));
    }

    private static StudentRepository instance;
}
