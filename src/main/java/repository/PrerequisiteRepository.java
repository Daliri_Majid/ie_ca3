package repository;

import java.sql.*;
import java.util.ArrayList;

public class PrerequisiteRepository {
    private PrerequisiteRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        Statement createTableStatement = con.createStatement();
        createTableStatement.executeUpdate(
                "\n" +
                        "CREATE TABLE IF NOT EXISTS Prerequisite\n" +
                        "(\n" +
                        " needy  char(7) NOT NULL,\n" +
                        " needed char(7) NOT NULL,\n" +
                        " PRIMARY KEY ( needy, needed ),\n" +
                        " FOREIGN KEY ( needy ) REFERENCES Lesson ( code ),\n" +
                        " FOREIGN KEY ( needed ) REFERENCES Lesson ( code )\n" +
                        ");"
        );
        createTableStatement.close();
        con.close();
    }

    public static PrerequisiteRepository getInstance() {
        if (instance == null)
            try {
                instance = new PrerequisiteRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in PrerequisiteRepository.create query.");
            }
        return instance;
    }

    public ArrayList<String> findNeeded(String needy) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT needed FROM Prerequisite prereq" +
                        " WHERE prereq.needy = ?;"
        );
        st.setString(1, needy);
        ResultSet rs = st.executeQuery();
        var needed = new ArrayList<String>();
        while (rs.next())
            needed.add(rs.getString(1));
        st.close();
        con.close();
        return needed;
    }

    public void insert(String needy, String needed) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "INSERT INTO Prerequisite(needy, needed) " +
                        "VALUES(?, ?);"
        );
        st.setString(1, needy);
        st.setString(2, needed);
        st.execute();
        st.close();
        con.close();
    }

    private static PrerequisiteRepository instance;
}
