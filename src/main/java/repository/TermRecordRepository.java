package repository;

import model.TermRecord;

import java.sql.*;
import java.util.ArrayList;

public class TermRecordRepository {
    private TermRecordRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        Statement createTableStatement = con.createStatement();
        createTableStatement.executeUpdate(
                "CREATE TABLE IF NOT EXISTS TermRecord\n" +
                        "(\n" +
                        " studentId        char(9) NOT NULL,\n" +
                        " term             int NOT NULL,\n" +
                        " gpa              float NOT NULL,\n" +
                        " totalUnits       int NOT NULL,\n" +
                        " totalPassedUnits int NOT NULL,\n" +
                        " PRIMARY KEY ( studentId, term ),\n" +
                        " FOREIGN KEY ( studentId ) REFERENCES Student ( id )\n" +
                        ");"
        );
        createTableStatement.close();
        con.close();
    }

    public static TermRecordRepository getInstance() {
        if (instance == null)
            try {
                instance = new TermRecordRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in TermRecordRepository.create query.");
            }
        return instance;
    }

    public int findLastTerm(String studentId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT MAX(term) FROM TermRecord record " +
                        "WHERE record.studentId = ?;"
        );
        st.setString(1, studentId);
        ResultSet rs = st.executeQuery();
        if (!rs.next())
        {
            st.close();
            con.close();
            return 0;
        }
        int result = rs.getInt(1);
        st.close();
        con.close();
        return result;
    }

    public ArrayList<TermRecord> findAllTerms(String studentId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "SELECT * FROM TermRecord record " +
                        "WHERE record.studentId = ? " +
                        "ORDER BY term ASC;"
        );
        st.setString(1, studentId);
        ResultSet rs = st.executeQuery();
        var terms = new ArrayList<TermRecord>();
        while (rs.next())
            terms.add(new TermRecord(rs.getString(1), rs.getInt(2), rs.getFloat(3),
                    rs.getInt(4), rs.getInt(5)));
        st.close();
        con.close();
        return terms;
    }

    public void updateStat(String studentId, int term, float gpa, int totalUnits,
                           int totalPassedUnits) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "UPDATE TermRecord record" +
                        " SET record.gpa = ?, record.totalUnits = ?, record.totalPassedUnits = ?" +
                        " WHERE record.studentId = ? AND record.term = ?;"
        );
        st.setFloat(1, gpa);
        st.setInt(2, totalUnits);
        st.setInt(3, totalPassedUnits);
        st.setString(4, studentId);
        st.setInt(5, term);
        st.executeUpdate();
        st.close();
        con.close();
    }

    public void insert(String studentId, int term, float gpa, int totalUnits,
                       int totalPassedUnits) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(
                "INSERT INTO TermRecord(studentId, term, gpa, totalUnits, totalPassedUnits) " +
                        "VALUES(?,?,?,?,?);"
        );
        st.setString(1, studentId);
        st.setInt(2, term);
        st.setFloat(3, gpa);
        st.setInt(4, totalUnits);
        st.setInt(5, totalPassedUnits);
        st.execute();
        st.close();
        con.close();
    }

    private static TermRecordRepository instance;
}

