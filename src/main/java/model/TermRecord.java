package model;

public class TermRecord {
    public TermRecord(String studentId, int term, float gpa, int totalUnits, int totalPassedUnits) {
        this.studentId = studentId;
        this.term = term;
        this.gpa = gpa;
        this.totalUnits = totalUnits;
        this.totalPassedUnits = totalPassedUnits;
    }

    public String studentId;
    public int term;
    public float gpa;
    public int totalUnits;
    public int totalPassedUnits;
}
