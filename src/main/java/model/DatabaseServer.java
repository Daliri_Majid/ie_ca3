package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DatabaseServer {
	public String getAllCourses() throws IOException {
		final String COURSES_LOCATION = "/api/courses";
		URL url = new URL(SERVER_URL + COURSES_LOCATION);
		return sendRequest(url);
	}

	public String getAllStudents() throws IOException {
		final String STUDENTS_LOCATION = "/api/students";
		URL url = new URL(SERVER_URL + STUDENTS_LOCATION);
		return sendRequest(url);
	}

	public String getStudentGrades(String studentId) throws IOException {
		final String GRADES_LOCATION = "/api/grades/";
		URL url = new URL(SERVER_URL + GRADES_LOCATION + studentId);
		return sendRequest(url);
	}

	private String sendRequest(URL url) throws IOException {
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		StringBuilder content = new StringBuilder();
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		in.close();
		con.disconnect();
		return content.toString();
	}

	private final String SERVER_URL = "http://138.197.181.131:5200";
}
