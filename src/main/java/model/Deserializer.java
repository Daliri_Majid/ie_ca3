package model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.util.ArrayList;
import java.util.Arrays;

public class Deserializer {
	public Deserializer() {
		mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.setVisibility(mapper.getSerializationConfig().getDefaultVisibilityChecker().
				withFieldVisibility(JsonAutoDetect.Visibility.ANY));
	}

	public ArrayList<Course> readCourses(String jsonData) throws JsonProcessingException {
		Course[] courses = mapper.readValue(jsonData, Course[].class);
		return new  ArrayList<>(Arrays.asList(courses));
	}

	public ArrayList<Student> readStudents(String jsonData) throws JsonProcessingException {
		Student[] students = mapper.readValue(jsonData, Student[].class);
		return new  ArrayList<>(Arrays.asList(students));
	}

	public ArrayList<CodeGradeTuple> readGrades(String jsonData) throws JsonProcessingException {
		CodeGradeTuple[] codeGradeTuples = mapper.readValue(jsonData, CodeGradeTuple[].class);
		return new ArrayList<>(Arrays.asList(codeGradeTuples));
	}

	private final ObjectMapper mapper;
}