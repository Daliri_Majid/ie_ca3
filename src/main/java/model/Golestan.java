package model;

import DTO.*;
import Util.JwtUtil;
import Util.PasswordEncoder;
import model.WeeklyScheduleItem.*;
import repository.*;
import service.RegisterRequest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.*;

public class Golestan {
	private Golestan(DatabaseServer databaseServer) throws IOException, SQLException {
		Deserializer deserializer = new Deserializer();
		ArrayList<Course> courseList = deserializer.readCourses(databaseServer.getAllCourses());
		for (Course course : courseList) {
			if (LessonRepository.getInstance().find(course.getCode()) == null)
				addNewLesson(course);
		}

		for (Course course : courseList) {
			if (CourseRepository.getInstance().find(course.getCode(), course.getClassCode()) == null) {
				CourseRepository.getInstance().insert(course);
				for (String code : course.getPrerequisites())
					PrerequisiteRepository.getInstance().insert(course.getCode(), code);
			}
		}

		String allStudents = databaseServer.getAllStudents();
		ArrayList<Student> studentList = deserializer.readStudents(allStudents);
		for (Student student : studentList) {
			if (StudentRepository.getInstance().findById(student.getId()) == null) {
				student.setPassword(PasswordEncoder.encode(student.getPassword()));
				StudentRepository.getInstance().insert(student);
				addGradeRecords(databaseServer, deserializer, student.getId());
			}
		}
	}

	public void resetPassword(String email, ArrayList<String> errors) throws SQLException, IOException {
		if (StudentRepository.getInstance().findByEmail(email) == null) {
			errors.add("دانشجویی با این ایمیل وجود ندارد!");
			return;
		}

		final String jwt = JwtUtil.getInstance().generateToken(email);
		final String frontUrl = "http://87.247.185.122:30620/resetPassword/";
		final String url =  frontUrl + jwt;
		String query = String.format("url=%s&email=%s",
				URLEncoder.encode(url, StandardCharsets.UTF_8),
				URLEncoder.encode(email, StandardCharsets.UTF_8));
		sendRequestToEmailApi(query, errors);
	}

	private void sendRequestToEmailApi(String query, ArrayList<String> errors) throws IOException {
		URL url = new URL("http://138.197.181.131:5200/api/send_mail" + "?" + query);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept", "application/json");
		if (con.getResponseCode() != 200)
			errors.add("!خطا در ارسال ایمیل");
	}

	public void registerStudent(RegisterRequest request, ArrayList<String> errors) throws SQLException {
		if (StudentRepository.getInstance().findByEmail(request.email) != null) {
			errors.add("دانشجویی با این ایمیل وجود دارد!");
			return;
		}
		if (StudentRepository.getInstance().findById(request.id) != null) {
			errors.add("دانشجویی با این شماره دانشجویی وجود دارد!");
			return;
		}

		final String DEFAULT_STATUS = "مشغول به تحصیل";
		final String DEFAULT_IMAGE = "http://138.197.181.131:5200/img/don_carlton.jpg";

		Student newStudent = new Student(
				request.id,
				request.name,
				request.lastName,
				request.email,
				PasswordEncoder.encode(request.password),
				request.birthDate,
				request.field,
				request.faculty,
				request.level,
				DEFAULT_STATUS,
				DEFAULT_IMAGE
		);

		StudentRepository.getInstance().insert(newStudent);
	}

	public StudentHistory getHistory(String id) throws SQLException {
		var termRecords = TermRecordRepository.getInstance().findAllTerms(id);
		StudentHistory studentHistory = new StudentHistory();
		for (var term : termRecords)
			studentHistory.addTermRecord(term);
		return studentHistory;
	}

	protected static TermRecordDTO getTermRecordDTO(TermRecord termRecord) throws SQLException {
		ArrayList<GradeRecordDTO> gradeRecordDTOS = new ArrayList<>();
		var gradeRecords = GradeRecordRepository.getInstance().
				findAllInTerm(termRecord.studentId, termRecord.term);
		for (var gradeRecord : gradeRecords) {
			Lesson lesson = LessonRepository.getInstance().find(gradeRecord.lessonCode);
			gradeRecordDTOS.add(new GradeRecordDTO(gradeRecord.grade, lesson.getCode(),
					lesson.getName(), lesson.getUnits()));
		}
		return new TermRecordDTO(termRecord, gradeRecordDTOS);
	}

	public static Golestan getInstance() throws SQLException, IOException {
		if (instance == null)
			instance = new Golestan(new DatabaseServer());
		return instance;
	}

	public void setPassword(String email, String password) throws SQLException {
		StudentRepository.getInstance().updatePassword(email, PasswordEncoder.encode(password));
	}

	public boolean authenticateStudent(String email, String password) {
		try {
			Student student = StudentRepository.getInstance().findByEmail(email);
			return student != null && student.getPassword().equals(PasswordEncoder.encode(password));
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return false;
	}

	private void addGradeRecords(DatabaseServer databaseServer, Deserializer deserializer,
								 String studentId) throws IOException, SQLException {
		String grades = databaseServer.getStudentGrades(studentId);
		ArrayList<CodeGradeTuple> gradesList = deserializer.readGrades(grades);
		if (gradesList.isEmpty())
			return;
		Collections.sort(gradesList);
		int currentTerm = 0;
		int totalUnits = 0;
		int totalPassedUnits = 0;
		float totalMarks = 0;
		for (var tuple : gradesList) {
			if (tuple.term != currentTerm) {
				currentTerm = tuple.term;
				addTermRecord(studentId, currentTerm);
				if (currentTerm != 1)
					updateTermRecord(studentId, currentTerm - 1, totalMarks / totalUnits, totalUnits, totalPassedUnits);
				totalUnits = 0;
				totalPassedUnits = 0;
				totalMarks = 0;
			}
			int units = LessonRepository.getInstance().find(tuple.code).getUnits();
			totalUnits += units;
			if (tuple.grade >= 10)
				totalPassedUnits += units;
			totalMarks += tuple.grade * units;
			GradeRecordRepository.getInstance().insert(tuple.code, studentId, tuple.term, tuple.grade);
		}
		updateTermRecord(studentId, currentTerm, totalMarks / totalUnits, totalUnits, totalPassedUnits);
	}

	private void updateTermRecord(String studentId, int term, float gpa, int totalUnits,
								  int totalPassedUnits) throws SQLException {
		TermRecordRepository.getInstance().updateStat(studentId, term, gpa, totalUnits, totalPassedUnits);
	}

	private void addTermRecord(String studentId, int term) throws SQLException {
		TermRecordRepository.getInstance().insert(studentId, term, 0, 0, 0);
	}

	private void addNewLesson(Course course) throws SQLException {
		Lesson lesson = new Lesson(course.getCode(), course.getUnits(), course.getName());
		LessonRepository.getInstance().insert(lesson);
	}

	public ArrayList<Course> getCourses(String id) throws SQLException {
		var courses = CourseRepository.getInstance().findNewCourses(id);
		for (var course : courses)
			course.signedUpNumber = WeeklyScheduleItemRepository.getInstance().findSignedUpNumber(
					course.getCode(), course.getClassCode());
		return courses;
	}

	public ArrayList<WeeklyScheduleItemDTO> getWeeklySchedule(String id) throws SQLException {
		ArrayList<WeeklyScheduleItemDTO> weeklyScheduleItemDTOS = new ArrayList<>();
		var items = WeeklyScheduleItemRepository.getInstance().findAllItems(id);
		for (var item : items)
			weeklyScheduleItemDTOS.add(getWeeklyScheduleItemDTO(item));
		return weeklyScheduleItemDTOS;
	}

	private WeeklyScheduleItemDTO getWeeklyScheduleItemDTO(WeeklyScheduleItem item) throws SQLException {
		return new WeeklyScheduleItemDTO(item, CourseRepository.getInstance().find(item.getCode(), item.getClassCode()));
	}

	public ArrayList<FinalScheduleItemDTO> getFinalSchedule(String id) throws SQLException {
		ArrayList<FinalScheduleItemDTO> weeklyScheduleItemDTOS = new ArrayList<>();
		var items = WeeklyScheduleItemRepository.getInstance().findFinalItems(id);
		for (var item : items)
			weeklyScheduleItemDTOS.add(getFinalScheduleItem(item));
		return weeklyScheduleItemDTOS;
	}

	private FinalScheduleItemDTO getFinalScheduleItem(WeeklyScheduleItem item) throws SQLException {
		return new FinalScheduleItemDTO(CourseRepository.getInstance().find(item.getCode(), item.getClassCode()));
	}

	public int getCurrentTerm(String id) throws SQLException {
		return TermRecordRepository.getInstance().findLastTerm(id) + 1;
	}

	public void addWeeklyScheduleItem(String id, String courseCode, String courseClassCode, String mode,
									  ArrayList<String> errors) throws SQLException {
		WeeklyScheduleItem item = new WeeklyScheduleItem(courseCode, courseClassCode, id,
				Status.NON_FINALIZED, (mode.equals("wait") ? Action.WAIT : Action.REGISTER));
		addToWeeklySchedule(id, item, errors);
	}

	private void addToWeeklySchedule(String id, WeeklyScheduleItem item, ArrayList<String> errors) throws SQLException {
		if (WeeklyScheduleItemRepository.getInstance().find(item.getCode(), item.getClassCode(), id) != null) {
			errors.add("هم اکنون در برنامه هفتگی قرار دارد!");
			return;
		}

		boolean noCollision = checkWeeklyScheduleCollision(id, item, errors);

		if (noCollision)
			WeeklyScheduleItemRepository.getInstance().insert(item);
	}

	private boolean checkPrerequisites(String id, ArrayList<WeeklyScheduleItem> items,
									   ArrayList<String> errors) throws SQLException {
		boolean result = true;
		for (var item : items) {
			if (item.status == Status.NON_FINALIZED && !checkPrerequisites(id, item, errors))
				result = false;
		}
		return result;
	}

	private boolean checkPrerequisites(String id, WeeklyScheduleItem item, ArrayList<String> errors) throws SQLException {
		boolean result = true;
		for (String neededCode : PrerequisiteRepository.getInstance().findNeeded(item.getCode())) {
			GradeRecord gradeRecord = GradeRecordRepository.getInstance().findLast(neededCode, id);
			if (gradeRecord == null || gradeRecord.grade < 10) {
				errors.add("عدم رعایت پیش نیاز: " + LessonRepository.getInstance().find(item.getCode()).getName() +
						" نیاز دارد به " + LessonRepository.getInstance().find(neededCode).getName());
				result = false;
			}
		}

		return result;
	}

	public void removeFromWeeklySchedule(String id, String code, String classCode) throws SQLException {
		var item = WeeklyScheduleItemRepository.getInstance().find(code, classCode, id);
		if (item.status == Status.NON_FINALIZED)
			WeeklyScheduleItemRepository.getInstance().remove(code, classCode, id);
		else
			updateAction(id, item, code, classCode);
	}

	private void updateAction(String id, WeeklyScheduleItem item, String code, String classCode) throws SQLException {
		Action newAction = Action.REMOVE;

		if (item.action == Action.REMOVE) {
			if (item.status == Status.WAITING)
				newAction = Action.WAIT;
			else if (item.status == Status.FINALIZED)
				newAction = Action.NONE;
		}

		WeeklyScheduleItemRepository.getInstance().updateAction(code, classCode, id,
				WeeklyScheduleItem.getActionString(newAction));
	}

	public void finalizeSchedule(String id, ArrayList<String> errors) throws SQLException {
		ArrayList<WeeklyScheduleItem> items = WeeklyScheduleItemRepository.getInstance().findAllItems(id);

		boolean standardNumberOfUnits = verifyWeeklyScheduleUnits(id, errors);
		boolean passedPrerequisites = checkPrerequisites(id, items, errors);

		if (standardNumberOfUnits && passedPrerequisites) {
			for (var item : items) {
				if (item.action == Action.REMOVE)
					WeeklyScheduleItemRepository.getInstance().remove(item.getCode(), item.getClassCode(),
							item.getStudentId());
				else if (item.status == Status.NON_FINALIZED) {
					final String newAction = WeeklyScheduleItem.getActionString(Action.NONE);
					String newStatus = " ";

					if (item.action == Action.REGISTER)
						newStatus = WeeklyScheduleItem.getStatusString(Status.FINALIZED);
					else if (item.action == Action.WAIT)
						newStatus = WeeklyScheduleItem.getStatusString(Status.WAITING);

					WeeklyScheduleItemRepository.getInstance().updateActionStatus(item.getCode(), item.getClassCode(),
							item.getStudentId(), newAction, newStatus);
				}
			}
		}
	}

	public void checkWaitingLists() throws SQLException {
		System.out.println("Checking Waiting Lists");
		var items = WeeklyScheduleItemRepository.getInstance().findAllWaitings();
		for (var item : items) {
			Course course = CourseRepository.getInstance().find(item.getCode(), item.getClassCode());
			course.signedUpNumber = WeeklyScheduleItemRepository.getInstance().findSignedUpNumber(
					course.getCode(), course.getClassCode());
			if (course.getSignedUpNumber() >= course.getCapacity()) {
				System.out.println("Error: Student " + item.getStudentId() + " cannot be added to "
						+ course.getCode() + " -> Not enough capacity");
				continue;
			}
			WeeklyScheduleItemRepository.getInstance().updateStatus(item.getCode(), item.getClassCode(),
					item.getStudentId(), WeeklyScheduleItem.getStatusString(Status.FINALIZED));
			System.out.println("Success: Student " + item.getStudentId() + " added to "
					+ course.getCode());
		}
	}

	private boolean verifyWeeklyScheduleUnits(String id, ArrayList<String> errors) throws SQLException {
		int sumUnits = WeeklyScheduleItemRepository.getInstance().getSumUnits(id);
		if (sumUnits < 12)
			errors.add("حداقل تعداد واحد رعایت نشده");
		else if (sumUnits > 20)
			errors.add("حداکثر تعداد واحد رعایت نشده");
		else
			return true;
		return false;
	}

	private boolean checkWeeklyScheduleCollision(String id, WeeklyScheduleItem newItem,
												 ArrayList<String> errors) throws SQLException {
		boolean noCollision = true;
		var courseKeys = WeeklyScheduleItemRepository.getInstance().findAllCourse(id);
		Course thisCourse = CourseRepository.getInstance().find(newItem.getCode(), newItem.getClassCode());
		for (CourseKey key : courseKeys) {
			Course otherCourse = CourseRepository.getInstance().find(key.code, key.classCode);
			if (thisCourse.collide(otherCourse, errors))
				noCollision = false;
		}
		return noCollision;
	}

	private static Golestan instance = null;
}
