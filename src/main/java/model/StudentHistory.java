package model;

import DTO.TermRecordDTO;

import java.sql.SQLException;
import java.util.ArrayList;

public class StudentHistory {
    public StudentHistory() {
        terms = new ArrayList<>();
        gpa = 0;
        totalPassedUnits = 0;
        totalMarks = 0;
        totalUnits = 0;
    }

    public void addTermRecord(TermRecord termRecord) throws SQLException {
        totalPassedUnits += termRecord.totalPassedUnits;
        totalMarks += termRecord.totalUnits * termRecord.gpa;
        totalUnits += termRecord.totalUnits;
        gpa = totalMarks / totalUnits;
        terms.add(Golestan.getTermRecordDTO(termRecord));
    }

    public ArrayList<TermRecordDTO> terms;
    public float gpa;
    public int totalPassedUnits;
    private float totalMarks;
    private int totalUnits;
}
