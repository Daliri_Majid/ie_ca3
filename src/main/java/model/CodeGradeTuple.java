package model;

public class CodeGradeTuple implements Comparable<CodeGradeTuple> {
	@Override
	public int compareTo(CodeGradeTuple other) {
		return this.term - other.term;
	}

	public String code;
	public float grade;
	public int term;
}
