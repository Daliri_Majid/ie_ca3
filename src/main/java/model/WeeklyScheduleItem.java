package model;

public class WeeklyScheduleItem {
	public WeeklyScheduleItem(String code, String classCode, String studentId, Status status, Action action) {
		this.code = code;
		this.classCode = classCode;
		this.studentId = studentId;
		this.status = status;
		this.action = action;
	}

	public WeeklyScheduleItem(String code, String classCode, String studentId, String status, String action) {
		this.code = code;
		this.classCode = classCode;
		this.studentId = studentId;
		setStatus(status);
		setAction(action);
	}

	private void setStatus(String status) {
		switch (status) {
			case "finalized":
				this.status = Status.FINALIZED;
				break;
			case "waiting":
				this.status = Status.WAITING;
				break;
			default:
				this.status = Status.NON_FINALIZED;
		}
	}

	private void setAction(String action) {
		switch (action) {
			case "remove":
				this.action = Action.REMOVE;
				break;
			case "register":
				this.action = Action.REGISTER;
				break;
			case "wait":
				this.action = Action.WAIT;
				break;
			default:
				this.action = Action.NONE;
		}
	}

	public static String getStatusString(Status status) {
		switch (status) {
			case FINALIZED:
				return "finalized";
			case WAITING:
				return "waiting";
			case NON_FINALIZED:
				return "non_finalized";
			default:
				return " ";
		}
	}

	public static String getActionString(Action action) {
		switch (action) {
			case REMOVE:
				return "remove";
			case REGISTER:
				return "register";
			case WAIT:
				return "wait";
			default:
				return "none";
		}
	}

	public String getStatus() {
		return getStatusString(status);
	}

	public String getAction() {
		return getActionString(action);
	}

	public String getClassCode() { return classCode; }
	public String getCode() { return code; }
	public String getStudentId() { return studentId; }

	private final String code;
	private final String classCode;
	private final String studentId;
	public Status status;
	public Action action;

	public enum Status {
		FINALIZED,
		NON_FINALIZED,
		WAITING
		}

	public enum Action {
		REGISTER,
		REMOVE,
		WAIT,
		NONE
	}
}