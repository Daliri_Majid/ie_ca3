package model;

public class GradeRecord {
	public GradeRecord(String lessonCode, String studentId, int term, float grade) {
		this.lessonCode = lessonCode;
		this.studentId = studentId;
		this.term = term;
		this.grade = grade;
	}

	public String lessonCode;
	public String studentId;
	public int term;
	public float grade;
}
