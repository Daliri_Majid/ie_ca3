package model;

import java.util.ArrayList;

public class Lesson {
	public Lesson() {}

	public Lesson(String code, int units, String name) {
		this.code = code;
		this.units = units;
		this.name = name;
		this.prerequisites = new ArrayList<>();
	}

	public String getCode() { return code; }
	public int getUnits() { return units; }
	public String getName() { return name; }
	public ArrayList<String> getPrerequisites() { return prerequisites;	}

	protected String code;
	protected String name;
	protected int units;
	protected ArrayList<String> prerequisites;
}
