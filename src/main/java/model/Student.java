package model;

public class Student {
	public Student() {}

	public Student(String id, String name, String secondName, String email, String password, String birthDate,
				   String field, String faculty, String level, String status, String image) {
		this.id = id;
		this.name = name;
		this.secondName = secondName;
		this.email = email;
		this.password = password;
		this.birthDate = birthDate;
		this.field = field;
		this.faculty = faculty;
		this.level = level;
		this.status = status;
		this.img = image;
	}

	public String getId() { return id; }
	public String getName() { return name; }
	public String getSecondName() { return secondName; }
	public String getBirthDate() { return birthDate; }
	public String getFaculty() { return faculty; }
	public String getField() { return field; }
	public String getLevel() { return level; }
	public String getStatus() { return status; }
	public String getImage() { return img; }
	public String getFullName() { return name + " " + secondName; }
	public String getEmail() { return email; }
	public String getPassword() { return password; }

	protected void setPassword(String password) { this.password = password; }

	private String id;
	private String name;
	private String secondName;
	private String email;
	private String password;
	private String birthDate;
	private String field;
	private String faculty;
	private String level;
	private String status;
	private String img;
}
