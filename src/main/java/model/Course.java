package model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Course extends Lesson {
	public Course() {}
	public Course(String code, String classCode, String name, int units, String type, String instructor,
				  	int capacity, String examStart, String examEnd, String classTime) {
		super(code, units, name);
		this.classCode = classCode;
		this.type = type;
		this.instructor = instructor;
		this.capacity = capacity;
		this.examTime = new ExamTime(examStart, examEnd);
		this.classTime = new ClassTime();
		this.classTime.setTime(classTime);
		this.signedUpNumber = 0;
	}

	public void setClassDays(ArrayList<String> classDays) {
		classTime.days = classDays;
	}

	public boolean collide(Course other, ArrayList<String> errors) {
		final String DELIMITER = " و ";
		boolean collide = false;

		if (this.classTime.collide(other.classTime)) {
			collide = true;
			errors.add("تداخل زمان کلاس: " + this.name + DELIMITER + other.name);
		}

		if (this.examTime.collide(other.examTime)) {
			collide = true;
			errors.add("تداخل زمان امتحان: " + this.name + DELIMITER + other.name);
		}

		return collide;
	}

	public static class ClassTime {
		public ClassTime() {
			days = new ArrayList<>();
		}

		@JsonCreator
		public ClassTime(@JsonProperty("days") ArrayList<String> days, @JsonProperty("time") String time) {
			this.days = days;
			setTime(time);
		}

		public boolean collide(ClassTime other) {
			if (!this.haveCommonDays(other))
				return false;

			return this.beginTime.compareTo(other.endTime) < 0 && other.beginTime.compareTo(this.endTime) < 0;
		}

		private boolean haveCommonDays(ClassTime other) {
			for (String day : this.days) {
				if (other.days.contains(day))
					return true;
			}

			return false;
		}

		public ArrayList<String> getDays() {
			return days;
		}

		public String getTime() { return time; }

		public void setTime(String time) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("H:mm");
			this.time = time;
			String[] splitTime = time.split("-");
			beginTime = LocalTime.parse(splitTime[0], formatter);
			endTime = LocalTime.parse(splitTime[1], formatter);
		}

		public ArrayList<String> days;
		public String time;
		@JsonIgnore
		private LocalTime beginTime;
		@JsonIgnore
		private LocalTime endTime;
	}

	public static class ExamTime {
		public ExamTime() { }

		public ExamTime(String start, String end) {
			this.start = LocalDateTime.parse(start);
			this.end = LocalDateTime.parse(end);
		}

		public boolean collide(ExamTime other) {
			return this.start.compareTo(other.end) < 0 && other.start.compareTo(this.end) < 0;
		}

		public String getStart() {
			return start.toString();
		}
		public String getEnd() { return end.toString(); }

		private LocalDateTime start;
		private LocalDateTime end;
	}

	public String getClassCode() { return classCode; }
	public String getInstructor() { return instructor; }
	public int getCapacity() { return capacity; }
	public String getType() { return type; }
	public ClassTime getClassTime() { return classTime; }
	public ExamTime getExamTime() { return examTime; }
	public int getSignedUpNumber() { return signedUpNumber; }

	private String type;
	private String classCode;
	private String instructor;
	private int capacity;
	private ClassTime classTime;
	private ExamTime examTime;
	protected int signedUpNumber;
}
