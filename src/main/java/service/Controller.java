package service;

import DTO.*;
import model.Course;
import model.Golestan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import repository.StudentRepository;

import javax.print.attribute.standard.Media;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

@RestController
public class Controller {
    @GetMapping(value = "/studentInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    public StudentInfoDTO getProfile(@RequestAttribute("id") String id) throws SQLException, IOException {
        return new StudentInfoDTO(StudentRepository.getInstance().findById(id),
                Golestan.getInstance().getHistory(id));
    }

    @GetMapping(value = "/weeklySchedule", produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<WeeklyScheduleItemDTO> getWeeklySchedule(@RequestAttribute("id") String id)
            throws SQLException, IOException {
        return Golestan.getInstance().getWeeklySchedule(id);
    }

    @GetMapping(value = "/finalSchedule", produces = MediaType.APPLICATION_JSON_VALUE)
    public FinalScheduleDTO getFinalSchedule(@RequestAttribute("id") String id) throws SQLException, IOException {
        return new FinalScheduleDTO(Golestan.getInstance().getFinalSchedule(id),
                Golestan.getInstance().getCurrentTerm(id));
    }

    @GetMapping(value = "/courses", produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Course> getCourses(@RequestAttribute("id") String id) throws SQLException, IOException {
        return Golestan.getInstance().getCourses(id);
    }

    @DeleteMapping(value = "/weeklySchedule/{code}/{classCode}")
    public void removeFromWeeklySchedule(@PathVariable String code, @PathVariable String classCode,
                                         @RequestAttribute("id") String id) throws SQLException, IOException {
        Golestan.getInstance().removeFromWeeklySchedule(id, code, classCode);
    }

    @PutMapping(value = "/weeklySchedule", produces = MediaType.APPLICATION_JSON_VALUE)
    public RequestResult updateWeeklySchedule(@RequestBody Map<String, String> body, @RequestAttribute("id") String id)
            throws SQLException, IOException {
        final Golestan golestan = Golestan.getInstance();
        String courseCode = body.get("code");
        String courseClassCode = body.get("classCode");
        String mode = body.get("mode");
        var errors = new ArrayList<String>();
        golestan.addWeeklyScheduleItem(id, courseCode, courseClassCode, mode, errors);
        return new RequestResult(errors);
    }

    @PostMapping(value = "/weeklySchedule", produces = MediaType.APPLICATION_JSON_VALUE)
    public RequestResult submitWeeklySchedule(@RequestAttribute("id") String id) throws SQLException, IOException {
        var errors = new ArrayList<String>();
        Golestan.getInstance().finalizeSchedule(id, errors);
        return new RequestResult(errors);
    }
}
