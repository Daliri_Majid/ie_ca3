package service;

import Util.JwtUtil;
import io.jsonwebtoken.JwtException;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import repository.StudentRepository;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;

public class JwtFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        final String requestTokenHeader = request.getHeader("Authorization");

        if (requestTokenHeader == null)
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);

        if (requestTokenHeader.startsWith("Bearer ")) {
            String jwtToken = requestTokenHeader.substring(7);
            try {
                final JwtUtil jwtUtil = JwtUtil.getInstance();
                String username = jwtUtil.getUsernameFromToken(jwtToken);
                if (username == null || jwtUtil.isTokenExpired(jwtToken))
                    throw new ResponseStatusException(HttpStatus.FORBIDDEN);
                final String id = getStudentId(username);
                if (id == null)
                    throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
                request.setAttribute("id", id);
            } catch (JwtException e) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);
            }
        } else {
            System.out.println("JWT Token does not begin with Bearer String");
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        chain.doFilter(servletRequest, servletResponse);
    }

    private String getStudentId(String username) {
        try {
            return StudentRepository.getInstance().findByEmail(username).getId();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
