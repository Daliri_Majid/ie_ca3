package service.ScheduledExecutor;

import model.Golestan;

import java.io.IOException;
import java.sql.SQLException;

public class CheckWaitingListReminder implements Runnable{
	@Override
	public void run()
	{
		try {
			Golestan.getInstance().checkWaitingLists();
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}
}
