package service;

import DTO.LoginResult;
import DTO.RequestResult;
import Util.JwtUtil;
import model.Golestan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

@RestController
public class AuthenticationController {
    private boolean authenticate(String username, String password) throws Exception {
        return Golestan.getInstance().authenticateStudent(username, password);
    }

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public LoginResult login(@RequestBody LoginRequest loginRequest) throws Exception {
        final String username = loginRequest.username;

        if (!authenticate(username, loginRequest.password))
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "نام کاربری یا رمز عبور اشتباه است.");

        final String jwt = JwtUtil.getInstance().generateToken(username);
        return new LoginResult(jwt);
    }

    @PostMapping(value = "/resetPassword", produces = MediaType.APPLICATION_JSON_VALUE)
    public void validateLink(@RequestBody ResetPasswordRequest request) throws SQLException, IOException {
        final JwtUtil jwtUtil = JwtUtil.getInstance();
        String email = jwtUtil.getUsernameFromToken(request.link);
        if (email == null || jwtUtil.isTokenExpired(request.link))
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        Golestan.getInstance().setPassword(email, request.newPassword);
    }

    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    public RequestResult register(@RequestBody RegisterRequest registerRequest) throws SQLException, IOException {
        var errors = new ArrayList<String>();
        Golestan.getInstance().registerStudent(registerRequest, errors);
        return new RequestResult(errors);
    }

    @PostMapping(value = "/forgotPassword", produces = MediaType.APPLICATION_JSON_VALUE)
    public RequestResult forgotPassword(@RequestBody Map<String, String> body) throws SQLException, IOException {
        var errors = new ArrayList<String>();
        Golestan.getInstance().resetPassword(body.get("email"), errors);
        return new RequestResult(errors);
    }
}
