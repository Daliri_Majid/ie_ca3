package service;

public class RegisterRequest {
    public String name;
    public String lastName;
    public String id;
    public String email;
    public String password;
    public String birthDate;
    public String field;
    public String faculty;
    public String level;
}
