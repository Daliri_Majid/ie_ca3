package DTO;

import model.Student;
import model.StudentHistory;

import java.util.ArrayList;

public class StudentInfoDTO {
    public StudentInfoDTO(Student student, StudentHistory studentHistory) {
        this.image = student.getImage();
        this.name = student.getFullName();
        this.id = student.getId();
        this.birthDate = student.getBirthDate();
        this.gpa = String.format("%.2f", studentHistory.gpa);
        this.passedUnits = studentHistory.totalPassedUnits;
        this.faculty = student.getFaculty();
        this.field = student.getField();
        this.level = student.getLevel();
        this.status = student.getStatus();
        termRecords = studentHistory.terms;
    }

    public String image;
    public String name;
    public String id;
    public String birthDate;
    public String gpa;
    public int passedUnits;
    public String faculty;
    public String field;
    public String level;
    public String status;
    public ArrayList<TermRecordDTO> termRecords;
}
