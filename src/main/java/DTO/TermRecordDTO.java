package DTO;

import model.TermRecord;

import java.util.ArrayList;

public class TermRecordDTO {
    public TermRecordDTO(TermRecord termRecord, ArrayList<GradeRecordDTO> gradeRecords) {
        this.term = termRecord.term;
        this.gpa = String.format("%.2f", termRecord.gpa);
        this.totalUnits = termRecord.totalUnits;
        this.totalPassedUnits = termRecord.totalPassedUnits;
        this.gradeRecords = gradeRecords;
    }

    public int term;
    public String gpa;
    public int totalUnits;
    public int totalPassedUnits;
    public ArrayList<GradeRecordDTO> gradeRecords;
}
