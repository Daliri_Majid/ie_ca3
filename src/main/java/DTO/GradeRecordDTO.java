package DTO;

public class GradeRecordDTO {
    public GradeRecordDTO(float grade, String lessonCode, String lessonName, int lessonUnits) {
        this.grade = grade;
        this.lessonCode = lessonCode;
        this.lessonName = lessonName;
        this.lessonUnits = lessonUnits;
    }

    public float grade;
    public String lessonCode;
    public String lessonName;
    public int lessonUnits;
}
