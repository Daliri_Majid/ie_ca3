package DTO;

public class LoginResult {
    public LoginResult(String jwt) {
        this.jwt = jwt;
    }

    public String jwt;
}

