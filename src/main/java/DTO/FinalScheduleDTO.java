package DTO;

import java.util.ArrayList;

public class FinalScheduleDTO {
    public FinalScheduleDTO(ArrayList<FinalScheduleItemDTO> items, int term) {
        this.items = items;
        this.term = term;
    }

    public ArrayList<FinalScheduleItemDTO> items;
    public int term;
}
