package DTO;

import model.Course;
import model.WeeklyScheduleItem;

public class WeeklyScheduleItemDTO {
    public WeeklyScheduleItemDTO(WeeklyScheduleItem item, Course course) {
        this.action = item.getAction();
        this.status = item.getStatus();
        this.code = item.getCode();
        this.classCode = item.getClassCode();
        this.name = course.getName();
        this.instructor = course.getInstructor();
        this.units = course.getUnits();
    }

    public String action;
    public String status;
    public String code;
    public String classCode;
    public String name;
    public String instructor;
    public int units;
}
