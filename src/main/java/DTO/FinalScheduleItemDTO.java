package DTO;

import model.Course;

import java.util.ArrayList;

public class FinalScheduleItemDTO {
    public FinalScheduleItemDTO(Course course) {
        this.name = course.getName();
        this.classTime = course.getClassTime().getTime();
        this.classDays = course.getClassTime().getDays();
        this.type = course.getType();
    }

    public String name;
    public String classTime;
    public ArrayList<String> classDays;
    public String type;
}
