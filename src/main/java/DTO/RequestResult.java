package DTO;

import java.util.ArrayList;

public class RequestResult {
    public RequestResult(ArrayList<String> errors) {
        this.errors = errors;
        this.success = errors.isEmpty();
    }

    public boolean success;
    public ArrayList<String> errors;
}
